include(FindPackageHandleStandardArgs)

set(LAME_FOUND FALSE)
set(LAME_INCLUDE_DIRS)
set(LAME_LIBRARIES)

# 查找 頭文件
find_path(LAME_INCLUDE_DIRS NAMES lame/lame.h)
if(NOT LAME_INCLUDE_DIRS)
    if(LAME_FIND_REQUIRED)
        message(FATAL_ERROR "LAME : Could not find lame/lame.h")
    else()
        message(WARNING "LAME : Could not find lame/lame.h")
    endif()
    return()
endif()


# 查找 庫文件
find_library(LAME_LIBRARIE_LAME NAMES mp3lame)
if(NOT LAME_LIBRARIE_LAME)
    if(LAME_FIND_REQUIRED)
        message(FATAL_ERROR "LAME : Could not find lib lame")
    else()
        message(WARNING "LAME : Could not find lib lame")
    endif()
    return()
endif()

list(APPEND LAME_LIBRARIES
    "${LAME_LIBRARIE_LAME}"
)
list(REMOVE_DUPLICATES LAME_INCLUDE_DIRS)
list(REMOVE_DUPLICATES LAME_LIBRARIES)

message(STATUS "Found LAME: ${LAME_LIBRARIES}")

# 設置 庫 信息
find_package_handle_standard_args(LAME
		DEFAULT_MSG
		LAME_INCLUDE_DIRS
        LAME_LIBRARIES
)
